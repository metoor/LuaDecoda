///////////////////////////////////////////////////////////////////////////
// DirDialog.cpp: implementation of the CDirDialog class.
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "DirDialog.h"
#include "shlobj.h"
#include "shlwapi.h"
#pragma comment(lib,"shlwapi.lib")

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define IDC_STATUS_DIRBROWSE 0x3743   // 这个是状态信息的STATIC控件的ID,可能在其他版本下会变.请用Spy++查看
#define DISMISS_DIRDIALOG 1

CONST CHAR* DIRDIALOG_REGKEYPATH = "Software\\OpenDirDlg";

BOOL RegistrySetKey(CString _FullKeyPathNoRoot, CString _Item, CString _Value)
{
	HKEY hKey;
	long lResult;

	lResult = RegCreateKeyEx(HKEY_CURRENT_USER, (LPCTSTR)_FullKeyPathNoRoot, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	if(lResult != ERROR_SUCCESS) return FALSE;

	lResult = RegSetValueEx(hKey, (LPCTSTR)_Item, 0, REG_SZ, (const BYTE *)(LPCTSTR)_Value, _Value.GetLength()+1);
	if(lResult != ERROR_SUCCESS) return FALSE;

	lResult = RegCloseKey(hKey);
	if(lResult != ERROR_SUCCESS) return FALSE;

	return TRUE;
}

BOOL RegistryGetKey(CString _FullKeyPathNoRoot, CString _Item, CString& _Value)
{
	HKEY hKey;
	long lResult;
	DWORD dwValueLen = MAX_PATH;

	lResult = RegCreateKeyEx(HKEY_CURRENT_USER, (LPCTSTR)_FullKeyPathNoRoot, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	if(lResult != ERROR_SUCCESS) return FALSE;

	lResult = RegQueryValueEx(hKey, (LPCTSTR)_Item, 0, NULL, (LPBYTE)_Value.GetBuffer(MAX_PATH),&dwValueLen);
	_Value.ReleaseBuffer();
	if(lResult != ERROR_SUCCESS) return FALSE;

	lResult = RegCloseKey(hKey);
	if(lResult != ERROR_SUCCESS) return FALSE;

	return TRUE;
}

// Callback function called by SHBrowseForFolder's browse control
// after initialization and when selection changes
int __stdcall CDirDialog::BrowseCtrlCallback(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
    CDirDialog* pDirDialogObj = (CDirDialog*)lpData;
    if (uMsg == BFFM_INITIALIZED)
    {
        if(pDirDialogObj->m_strSelDir.IsEmpty())
		{
			::GetCurrentDirectory(MAX_PATH,pDirDialogObj->m_strSelDir.GetBuffer(MAX_PATH));
			pDirDialogObj->m_strSelDir.ReleaseBuffer();
		}
		::SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)(LPCTSTR)(pDirDialogObj->m_strSelDir));
        if(!pDirDialogObj->m_strWindowTitle.IsEmpty())
            ::SetWindowText(hwnd, (LPCTSTR) pDirDialogObj->m_strWindowTitle);
    }
    else if(uMsg == BFFM_SELCHANGED)
    {
        LPITEMIDLIST pidl = (LPITEMIDLIST) lParam;
		TCHAR szSelection[MAX_PATH] = {0};
		pDirDialogObj->m_Path.Empty();
        if(::SHGetPathFromIDList(pidl, szSelection))
		{
			pDirDialogObj->m_Path = szSelection;
		}
		if(pDirDialogObj->m_bStatus)
		{
			if (GetDlgItem(hwnd,IDC_STATUS_DIRBROWSE))
				::PathSetDlgItemPath(hwnd,IDC_STATUS_DIRBROWSE,szSelection);
			else
				::SendMessage(hwnd, BFFM_SETSTATUSTEXT , 0, (LPARAM)szSelection);
		}
		::SendMessage(hwnd, BFFM_ENABLEOK, 0, !pDirDialogObj->m_Path.IsEmpty());
    }
	else if (uMsg == BFFM_VALIDATEFAILED)
	{
		CString strSelection = (TCHAR*)lParam;
		if (-1 == ::PathGetDriveNumber(strSelection))
		{
			CString strSelDir = pDirDialogObj->m_Path;
			strSelDir.TrimRight("\\/");
			if (!strSelDir.IsEmpty()) {
				::PathRemoveFileSpecA(strSelDir.GetBuffer());
				strSelDir.ReleaseBuffer();
				strSelection = strSelDir + "\\" + strSelection;
			}
		}

		if (!::PathFileExists(strSelection)) {
			return DISMISS_DIRDIALOG;
		}
		else {
			SendMessage(hwnd,BFFM_SETSELECTION,(WPARAM)TRUE,(LPARAM)(LPCTSTR)strSelection);
			pDirDialogObj->m_EditBoxInputOK = TRUE;
		}
	}
  return 0;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirDialog::CDirDialog(CString strRegItem/* = ""*/,
					   CString strSelDir/* = ""*/,
					   CString strTitle/* = "请选择一个目录"*/,
					   CString strWindowTitle/* = "浏览"*/)
: m_RegItem(strRegItem)
, m_strSelDir(strSelDir)
, m_strTitle(strTitle)
, m_strWindowTitle(strWindowTitle)
{
	m_UseReg = TRUE;
    m_Path.Empty();
    m_InitDir.Empty();
    m_ImageIndex = 0;
	m_EditBoxInputOK = FALSE;
}

CDirDialog::~CDirDialog()
{
}

BOOL CDirDialog::DoModal(CWnd *pwndParent/* = NULL*/)
{
    if(!m_strSelDir.IsEmpty())
    {
		m_strSelDir.Replace('/','\\');
		::PathRemoveBackslash(m_strSelDir.Trim().GetBuffer(MAX_PATH));
		m_strSelDir.ReleaseBuffer();
    }
	else if (!m_RegItem.IsEmpty() && m_UseReg)
	{
		RegistryGetKey(DIRDIALOG_REGKEYPATH,m_RegItem,m_strSelDir);
	}

    LPITEMIDLIST pidl;
    ZeroMemory((PVOID)&m_bInfo,sizeof(BROWSEINFO));

    if (!m_InitDir.IsEmpty())
    {
        OLECHAR       olePath[MAX_PATH];
        ULONG         chEaten;
        ULONG         dwAttributes;
        HRESULT       hr;
        LPSHELLFOLDER pDesktopFolder;
        //
        // Get a pointer to the Desktop's IShellFolder interface.
        //
        if (SUCCEEDED(SHGetDesktopFolder(&pDesktopFolder)))
        {
            //
            // IShellFolder::ParseDisplayName requires the file name be in Unicode.
            //
            MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, m_InitDir, -1,
                                olePath, MAX_PATH);

            //
            // Convert the path to an ITEMIDLIST.
            //
            hr = pDesktopFolder->ParseDisplayName(NULL,
                                                NULL,
                                                olePath,
                                                &chEaten,
                                                &pidl,
                                                &dwAttributes);
            if (SUCCEEDED(hr))
				m_bInfo.pidlRoot = pidl;
        }
    }
    m_bInfo.hwndOwner = pwndParent == NULL ? NULL : pwndParent->GetSafeHwnd();
    m_bInfo.pszDisplayName = m_InitDir.GetBuffer(MAX_PATH);
    m_bInfo.lpszTitle = m_strTitle;
    m_bInfo.ulFlags = BIF_RETURNFSANCESTORS
                    | BIF_RETURNONLYFSDIRS | BIF_USENEWUI
/*                    | (m_bStatus ? BIF_STATUSTEXT : 0)
					| (m_bEditBox ? BIF_EDITBOX|BIF_VALIDATE : 0)*/;

    m_bInfo.lpfn = BrowseCtrlCallback;  // address of callback function
    m_bInfo.lParam = (LPARAM)this;      // pass address of object to callback function

	pidl = ::SHBrowseForFolder(&m_bInfo);
	m_ImageIndex = m_bInfo.iImage;
	m_InitDir.ReleaseBuffer();

	if (pidl) {
		::SHGetPathFromIDList(pidl, m_Path.GetBuffer(MAX_PATH));
		m_Path.ReleaseBuffer();
	}
	if (m_EditBoxInputOK){
		pidl = (LPITEMIDLIST)1;
		m_EditBoxInputOK = FALSE;
	}

	if (!m_RegItem.IsEmpty() && m_UseReg) {
		RegistrySetKey(DIRDIALOG_REGKEYPATH,m_RegItem,m_Path);
	}
    return (pidl != NULL);
}