////////////////////////////////////////////////////////////////////////
// DirDialog.h: interface for the CDirDialog class.
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRDIALOG_H__62FFAC92_1DEE_11D1_B87A_0060979CDF6D__INCLUDED_)
#define AFX_DIRDIALOG_H__62FFAC92_1DEE_11D1_B87A_0060979CDF6D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

BOOL RegistrySetKey(CString _FullKeyPathNoRoot, CString _Item, CString _Value);
BOOL RegistryGetKey(CString _FullKeyPathNoRoot, CString _Item, CString& _Value);

class CDirDialog
{
public:

    CDirDialog(CString strRegItem = "", CString strSelDir = "",
		CString strTitle = "��ѡ��һ��Ŀ¼", CString strWindowTitle = "���");
    virtual ~CDirDialog();

    BOOL DoModal(CWnd *pwndParent = NULL);

	void SetWindowTitle(CString title){m_strWindowTitle = title;}
	void SetTitle(CString title){m_strTitle = title;}
	void SetSelDir(CString Dir){m_strSelDir = Dir;}
	void SetRootDir(CString Dir){m_InitDir = Dir;}
	void SetUseRegistry(BOOL bFlag){m_UseReg = bFlag;}

	CString GetWindowText(){return m_strWindowTitle;}
	CString GetTitle(){return m_strTitle;}
	CString GetPathname(){return m_Path;}

protected:
	BOOL m_UseReg;
	CString m_RegItem;
    CString m_strWindowTitle;
    CString m_Path;
    CString m_InitDir;
    CString m_strSelDir;
    CString m_strTitle;
    int  m_ImageIndex;
	BOOL m_bStatus;
	BOOL m_bEditBox;
	BROWSEINFO m_bInfo;
	BOOL m_EditBoxInputOK;

private:

    static int __stdcall CDirDialog::BrowseCtrlCallback(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData);
};

#endif // !defined(AFX_DIRDIALOG_H__62FFAC92_1DEE_11D1_B87A_0060979CDF6D__INCLUDED_)

