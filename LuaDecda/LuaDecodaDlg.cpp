// LuaDecodaDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "LuaDecoda.h"
#include "LuaDecodaDlg.h"
extern "C"
{
#include "luadec.h"
};
#include "DirDialog.h"
#include <vector>
#include <string>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CLuaDecodaDlg 对话框




CLuaDecodaDlg::CLuaDecodaDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLuaDecodaDlg::IDD, pParent)
	, m_InDir(_T(""))
	, m_OutDir(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLuaDecodaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_IN, m_InDir);
	DDX_Text(pDX, IDC_EDIT_OUT, m_OutDir);
}

BEGIN_MESSAGE_MAP(CLuaDecodaDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_IN, &CLuaDecodaDlg::OnBnClickedButtonIn)
	ON_BN_CLICKED(IDC_BUTTON_OUT, &CLuaDecodaDlg::OnBnClickedButtonOut)
	ON_BN_CLICKED(IDC_BUTTON_DEC, &CLuaDecodaDlg::OnBnClickedButtonDec)
END_MESSAGE_MAP()


// CLuaDecodaDlg 消息处理程序

BOOL CLuaDecodaDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CLuaDecodaDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CLuaDecodaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CLuaDecodaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CLuaDecodaDlg::OnBnClickedButtonIn()
{
	CDirDialog _Dir("LuaDecodaV1.0ByZhupf_In",m_InDir);
	if (IDOK == _Dir.DoModal(this)) {
		m_InDir = _Dir.GetPathname();
		UpdateData(FALSE);
	}
}

void CLuaDecodaDlg::OnBnClickedButtonOut()
{
	CDirDialog _Dir("LuaDecodaV1.0ByZhupf_Out",m_OutDir);
	if (IDOK == _Dir.DoModal(this)) {
		m_OutDir = _Dir.GetPathname();
		UpdateData(FALSE);
	}
}

VOID FindAllFile(const string& _RootName,const string& _DirName,
				 const string& _Mark,vector<string>& _FileList, BOOL _FindSubDir = FALSE)
{
	WIN32_FIND_DATAA _FindData;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	string _FindPath = _DirName.empty()?(_RootName+"\\"+_Mark):(_RootName+"\\"+_DirName+"\\"+_Mark);
	hFind = FindFirstFileA(_FindPath.c_str(), &_FindData);
	if (hFind == INVALID_HANDLE_VALUE)
		return;

	string _FindFileName;
	do {
		_FindFileName = _FindData.cFileName;
		if (_FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY && _FindSubDir) {
			if (_FindFileName=="." || _FindFileName=="..") {
				continue;
			}
			else {
				string _NewDir = _DirName.empty()?_FindFileName:_DirName+"\\"+_FindFileName;
				FindAllFile(_RootName,_NewDir,_Mark,_FileList,_FindSubDir);
			}
		}
		else {
			string _File = _DirName.empty()?_FindFileName:_DirName+"\\"+_FindFileName;
			_FileList.push_back(_File);
		}
	} while (FindNextFileA(hFind, &_FindData) != 0);
	FindClose(hFind);
}

bool FileGetData(const string& _FileName,string& _Buff,int pos,int len)
{
	bool _ret = false;
	FILE* _pBinFile = fopen(_FileName.c_str(), "rb");
	if(_pBinFile){
		fseek(_pBinFile, 0, SEEK_END);
		UINT _FileSize = ftell(_pBinFile);
		if ((pos + len) < _FileSize) {
			fseek(_pBinFile, pos, SEEK_SET);
			_Buff.resize(len);
			_ret = true;
			if(!fread((void*)_Buff.c_str(), len, 1, _pBinFile) == 1) {
				_Buff = "";
				_ret = false;
			}
			fclose(_pBinFile);
		}
	}
	return _ret;
}

BOOL IsNeedDec(const string& _FileName)
{
	string _Buff;
	if (FileGetData(_FileName,_Buff,0,1) && !_Buff.empty() && _Buff[0] == 0x1b) {
		return TRUE;
	}
	return FALSE;
}

void CLuaDecodaDlg::OnBnClickedButtonDec()
{
	UpdateData(TRUE);
	if (m_InDir.IsEmpty() || !PathFileExists((LPCTSTR)m_InDir)) {
		AfxMessageBox("输入目录不存在");
		return;
	}
	::MakeSureDirectoryPathExists((LPCTSTR)(m_OutDir+"\\"));
	if (m_OutDir.IsEmpty() || !PathFileExists((LPCTSTR)m_OutDir)) {
		AfxMessageBox("输出目录不存在,且创建失败");
		return;
	}
	string _ProgName = "LuaDecoda by zhupf";
	vector<string> _FileList;
	FindAllFile((LPCTSTR)m_InDir,"","*.*",_FileList,TRUE);
	if (_FileList.empty()) {
		AfxMessageBox("输入目录是空目录");
		return;
	}
	string _InDir = string((LPCTSTR)m_InDir) + "\\";
	string _OutDir = string((LPCTSTR)m_OutDir) + "\\";
	if (m_InDir.MakeLower().Trim("\\/") != m_OutDir.MakeLower().Trim("\\/")) {
		string _ReadFile;
		string _SaveFile;
		FILE* _OutFile;
		for (INT _Idx = 0; _Idx < _FileList.size(); _Idx++) {
			_ReadFile = _InDir + _FileList[_Idx];
			_SaveFile = _OutDir + _FileList[_Idx];
			::MakeSureDirectoryPathExists(_SaveFile.c_str());
			if (IsNeedDec(_ReadFile)) {
				_OutFile = fopen(_SaveFile.c_str(), "w");
				set_debugfile(_OutFile);
				set_errorfile(_OutFile);
				luafiledec((char*)_ProgName.c_str(),(char*)_ReadFile.c_str());
				fclose(_OutFile);
			}
			else {
				::MoveFileEx(_ReadFile.c_str(),_SaveFile.c_str(),MOVEFILE_REPLACE_EXISTING|MOVEFILE_WRITE_THROUGH);
			}
		}
	}
	else {
		AfxMessageBox("输入目录和输出目录不能相同");
		return;
		string _ReadFile;
		string _SaveFile;
		FILE* _OutFile;
		for (INT _Idx = 0; _Idx < _FileList.size(); _Idx++) {
			_ReadFile = _InDir + _FileList[_Idx];
			_SaveFile = _OutDir + _FileList[_Idx];
			if (IsNeedDec(_ReadFile)) {
				_ReadFile = _ReadFile + ".tmp";
				::MoveFileEx(_SaveFile.c_str(),_ReadFile.c_str(),MOVEFILE_REPLACE_EXISTING|MOVEFILE_WRITE_THROUGH);
				_OutFile = fopen(_SaveFile.c_str(), "w");
				set_debugfile(_OutFile);
				set_errorfile(_OutFile);
				luafiledec((char*)_ProgName.c_str(),(char*)_ReadFile.c_str());
				fclose(_OutFile);
				::DeleteFile(_ReadFile.c_str());
			}
		}
	}
	AfxMessageBox("转换完成!");
}

BOOL CLuaDecodaDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN && 
		(pMsg->wParam == VK_ESCAPE || 
		pMsg->wParam == VK_RETURN))
	{
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}
