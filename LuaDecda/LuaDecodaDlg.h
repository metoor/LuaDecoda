// LuaDecodaDlg.h : 头文件
//

#pragma once


// CLuaDecodaDlg 对话框
class CLuaDecodaDlg : public CDialog
{
// 构造
public:
	CLuaDecodaDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_LUADECODA_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public:
	CString m_InDir;
	CString m_OutDir;

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonIn();
	afx_msg void OnBnClickedButtonOut();
	afx_msg void OnBnClickedButtonDec();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
